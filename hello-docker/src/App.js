import React, { Component } from 'react';
import logo from './resources/logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="logo-container">
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <p>
            Hello Local Variables from Docker!
          </p>
          <a
              className="App-link"
              href="https://reactjs.org/blog/2016/07/22/create-apps-with-no-configuration.html"
              target="_blank"
              rel="noopener noreferrer"
          >
            This project was created using the React CLI
          </a>
          <br />
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn more about React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
