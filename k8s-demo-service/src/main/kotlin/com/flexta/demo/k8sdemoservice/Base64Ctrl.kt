package com.flexta.demo.k8sdemoservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class Base64Ctrl {
    @Autowired
    lateinit var base64Service: Base64Service

    @Autowired
    lateinit var ipAddressService: IpAddressService

    @PostMapping("/encode")
    fun encodeBase64(@RequestBody body: Request): Response = Response(base64Service.encode(body.message), ipAddressService.findIpAddress())

    @PostMapping("/decode")
    fun decodeBase64(@RequestBody body: Request): Response = Response(base64Service.decode(body.message), ipAddressService.findIpAddress())

}