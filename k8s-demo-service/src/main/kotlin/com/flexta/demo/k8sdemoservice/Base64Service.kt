package com.flexta.demo.k8sdemoservice

import org.springframework.stereotype.Service
import java.util.*

@Service
class Base64Service {
    fun encode(message: String): String = Base64.getEncoder().encodeToString(message.toByteArray())

    fun decode(message: String): String = String(Base64.getDecoder().decode(message))
}
