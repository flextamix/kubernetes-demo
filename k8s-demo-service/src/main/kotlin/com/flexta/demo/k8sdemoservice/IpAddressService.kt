package com.flexta.demo.k8sdemoservice

import org.springframework.stereotype.Service
import java.net.InetAddress

@Service
class IpAddressService {
    fun findIpAddress(): String = InetAddress.getLocalHost().hostAddress
}