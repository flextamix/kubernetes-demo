package com.flexta.demo.k8sdemoservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class K8sDemoServiceApplication

fun main(args: Array<String>) {
    runApplication<K8sDemoServiceApplication>(*args)
}
