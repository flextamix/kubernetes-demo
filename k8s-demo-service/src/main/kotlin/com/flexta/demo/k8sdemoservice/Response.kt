package com.flexta.demo.k8sdemoservice

import com.fasterxml.jackson.annotation.JsonProperty

data class Response(@JsonProperty var message: String, @JsonProperty var ip: String)