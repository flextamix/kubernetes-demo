package com.flexta.demo.k8sdemoservice

import com.fasterxml.jackson.annotation.JsonProperty

data class Request(@JsonProperty val message: String)