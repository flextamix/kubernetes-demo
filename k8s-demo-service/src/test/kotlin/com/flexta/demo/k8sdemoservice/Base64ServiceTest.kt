package com.flexta.demo.k8sdemoservice

import org.junit.Assert
import org.junit.Test

internal class Base64ServiceTest {
    var base64Service = Base64Service()

    @Test
    fun roundRobin() {
        val test = "test"
        val encoded = base64Service.encode(test)
        val decoded = base64Service.decode(encoded)
        Assert.assertEquals(test, decoded)
    }
}