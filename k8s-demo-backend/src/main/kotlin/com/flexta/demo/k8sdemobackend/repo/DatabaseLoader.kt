package com.flexta.demo.k8sdemobackend.repo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class DatabaseLoader : CommandLineRunner {
    @Autowired
    lateinit var languageRepo : LanguageRepo

    override fun run(vararg args: String) {
        addLanguagesFromCsv()
    }

    private fun addLanguagesFromCsv() {
        val lines = javaClass.getResource("/languages.csv")
                             .readText()
                             .split("\n")

        lines.forEach{ line ->
            this.languageRepo.save(parseLanguage(line))
        }
    }

    private fun parseLanguage(line: String): Language {
        val tokens = line.split("|")
        return Language(tokens[0],
                tokens[1],
                Integer.parseInt(tokens[2]))
    }
}