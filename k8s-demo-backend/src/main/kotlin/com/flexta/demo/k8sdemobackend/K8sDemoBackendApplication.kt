package com.flexta.demo.k8sdemobackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class K8sDemoBackendApplication

fun main(args: Array<String>) {
    runApplication<K8sDemoBackendApplication>(*args)
}
