package com.flexta.demo.k8sdemobackend.repo

import org.springframework.data.repository.PagingAndSortingRepository

interface LanguageRepo : PagingAndSortingRepository<Language, Long>