package com.flexta.demo.k8sdemobackend.repo

import javax.persistence.*

@Entity
data class Language (@Column val name: String,
                     @Column val skillLevel: String,
                     @Column val yearsExperience: Int) {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null
}