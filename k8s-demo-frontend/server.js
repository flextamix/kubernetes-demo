const axios = require('axios');
const express = require('express');

const serviceUrl = 'http://demo-service:8080';
const backendUrl = 'http://demo-backend:8080/api';

const bodyParser = require('body-parser');
const app = express();

const port = process.env.PORT || 5000;
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/languages', (req, res) => {
  console.log('/api/languages endpoint received GET request');
  console.log('Forwarding request onto: ' + backendUrl + '/languages');
  axios.get(backendUrl + '/languages')
    .then(result => {
      console.log('Received response from ' + backendUrl + '/languages: ' + JSON.stringify(result.data));
      let languages = result.data._embedded.languages;
      res.send({languages: languages});
    })
});

app.post('/api/encode', (req, res) => {
  console.log('/api/encode endpoint received POST request with body : ' + JSON.stringify(req.body));
  console.log('Forwarding request onto: ' + serviceUrl + '/encode');
  axios.post(serviceUrl + "/encode", req.body)
    .then(result => {
      console.log('Received response from ' + serviceUrl + '/encode: ' + JSON.stringify(result.data));
      res.send(
        result.data
      );
    });
});

app.post('/api/decode', (req, res) => {
  console.log('/api/decode endpoint received POST request with body : ' + JSON.stringify(req.body));
  console.log('Forwarding request onto: ' + serviceUrl + '/decode');
  axios.post(serviceUrl + "/decode", req.body)
    .then(result => {
      console.log('Received response from ' + serviceUrl + '/decode: ' + JSON.stringify(result.data));
      res.send(
        result.data
      );
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));