import React from 'react';
import axios from 'axios';
import GridListTile from "@material-ui/core/GridListTile";
import GridList from "@material-ui/core/GridList";
import ListSubheader from "@material-ui/core/ListSubheader";
import GridListTileBar from "@material-ui/core/GridListTileBar";

class Base64 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  encode = event => {
    const encode = this.state.encode;
    axios.post("/api/encode", {message: encode})
      .then(res => {
        this.setState({
          encoded: res.data.message,
          ip: res.data.ip
        })
      });
  };

  decode = event => {
    const encoded = this.state.encoded;
    axios.post("/api/decode", {message: encoded})
      .then(res => {
        this.setState({
          decoded: res.data.message,
          ip: res.data.ip
        })
      });
  };

  handleChange = event => {
    this.setState({encode: event.target.value});
  };

  render() {
    return (
      <GridList cols={3}>
        <GridListTile key="Subheader" cols={3} style={{height: 'auto'}}>
          <ListSubheader component="div">Backend Encoding Service</ListSubheader>
        </GridListTile>
        <GridListTile>
          <GridListTileBar
            title="Encoding Service"
          />
          <input type="text" value={this.state.encode} onChange={this.handleChange}/>
          <button type="submit" onClick={this.encode}>Encode</button>
        </GridListTile>
        <GridListTile>
          <GridListTileBar
            title="Encoded String"
          />
          <label>{this.state.encoded}</label><br/>
          <button type="submit" onClick={this.decode}>Decode</button>
        </GridListTile>
        <GridListTile>
          <GridListTileBar
            title="Decoded String"
          />
          <label>{this.state.decoded}</label>
        </GridListTile>
      </GridList>
    );
  }
}

export default Base64;