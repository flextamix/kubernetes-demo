import React, {Component} from 'react';
import './App.css';
import Languages from "./languages/Languages";
import Base64 from "./base64/Base64";
import {withStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: '50px',
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class App extends Component {

  render() {
    return (
      <div className="App">
        <Grid container spacing={32}>
          <Grid item xs={12}>
            <Paper>
              <Languages/>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Base64/>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(App);
