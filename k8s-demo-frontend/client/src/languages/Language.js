import React from 'react';
import {TableCell, TableRow} from "@material-ui/core";

class Language extends React.Component {
  render() {
    return (
      <TableRow>
        <TableCell>{this.props.language.name}</TableCell>
        <TableCell>{this.props.language.skillLevel}</TableCell>
        <TableCell>{this.props.language.yearsExperience}</TableCell>
      </TableRow>
    );
  }
}

export default Language;