import React from 'react';
import axios from 'axios';
import Table from '@material-ui/core/Table';

import Language from './Language';
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

class Languages extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      languages: [],
    };
  }

  componentDidMount() {
    axios.get('/api/languages')
      .then(res => {
        this.setState({languages: res.data.languages});
      })
  }

  render() {
    const languages = this.state.languages.map(language =>
      <Language key={language._links.self.href} language={language}/>
    );

    return (
      <Table>
        <TableBody>
        <TableRow>
          <TableCell>Language</TableCell>
          <TableCell>Skill Level</TableCell>
          <TableCell>Years Experience</TableCell>
        </TableRow>
        {languages}
        </TableBody>
      </Table>
    );
  }
}

export default Languages;