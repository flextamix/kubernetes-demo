# Presentation Notes

### Slide 4
* Installation
	* Docker Desktop for Windows
		* Uses Hyper-V or VirtualBox if you don't have Professional
	* Docker Desktop can install a local K8s cluster
		* What I will be using for this talk
	* You'll want local admin rights for installation/setup process

### Slide 5
* Read from slide
* Additional terms:
	* Dockerize/Containerize
		* Prepare application and create installation steps as to run your application in a container

### Slide 7
* Show `http://hub.docker.com`
	* Search for Java image
* Not difficult to setup Docker Repo
	* Need a cert

### Slide 8
* These are my most commonly used commands
	* Prune - deletes dangling images OR not running containers

### Slide 9
* These commands are about image management 
* A lot of these should be done by your CI/CD pipeline

### Slide 10 
* Document Converter Example
	* Multitenet service used to convert files
	* Needs to be scalable 
	* Java with Spring Boot
* Dockerfiles:
	* Dockerfile Keywords
		* `FROM` – The base image
			* Image name : tag name
		* `WORKDIR` – Where are we
		* `ENV` – Environment variables
		* `COPY/ADD` – Move files into the image
			* These are fairly interchangable
		* `RUN` – Run commands
		* `CMD/ENTRYPOINT` – Kick off the app
	* Image Layers
		* Cached until a change is detected

### Slide 11
#### Building:
* Powershell
* `cd git/kubernetes-demo/hello-docker`
* `docker build -t hello-docker .`
	* `-t` : Tag for the built image. Pattern is name:tag, if no tag is supplied `latest` is used
* docker images ls

#### Running:
* Powershell
* `docker run -it -p 3000:3000 --rm hello-docker`
	* `-it` : Interactive tty attached to container
	* `-d` : Run detached, basically opposite to -it
	* `-p` : Port mapping, local:container
	* `--rm` : Remove container when done running

### Slide 12
#### Dockerfile Best Practices
Read from slide.

### Slide 13
* Now that you've containerized your application, let's orchestrate it with K8s
* Can have a cluster in a cloud service or on "bare metal" (includes a private cloud)
* Use `kubeadm` to setup a local cluster if you must
* Show `https://kubernetes.io/docs/tutorials/kubernetes-basics/` and talk about cluster layout

### Slide 14
Read from slide.

### Slide 15
* There's an object type for just about anything you can think you may need

### Slide 16
* I never use create
* Apply and Delete allow you to easily create or remove objects
* Get/Describe should give you all you need to troubleshot
	* `get -o yaml` will output the full config

### Slide 17
* One pod is equivilant to one container
	* You can add a "Sidecar" container, but this is not the norm
* Deployments should be used for anything that you will ever want to scale
* Show the `deployment.yaml` for document converter.

### Slide 18
* I've only worked with NodePort
* Note enitirely clear how it chooses a pod to route to
* Would recommend looking into load balancers if doing bare metal
* Show the `service.yaml` for document converter.

### Slide 19
* Helpful but not be all end all
* Cloud providers have specific implementations
* Nginx has an implementation for bare metal clusters
* We still need to put an nginx server in front of ours due to issues with getting origin IP

### Slide 20
#### K8s Demo
* Wanted to give demo on multinode cluster, couldn't get setup due to extenuating circumstances
* Two backend services
	* `Kotlin` + `Spring Boot`
* Frontend
	* `React` + `Express`

* K8s Demo Backend
	* Database access layer
	* Uses `JPA` for `REST` endpoints
		* Show `DatabaseLoader.kt`, `languages.csv`
	* Run server
		* Go to browser, goto `localhost:8080/api/languages`, `localhost:8080/api/languages/1`, etc. 
	* Create Objects
		* `cd git/kubernetes-demo/k8s-demo-backend/`
		* `kubectl apply -f service.yaml`
		* `kubectl apply -f pod.yaml`
	* Check server
		* `kubectl get pods`
		* `kubectl get svc`
			* Get nodeport
		* Same as above to check end points

* K8s Demo Service
	* Encoding service
		* Base64 Encodes/Decodes things
	* Show `Base64Ctrl.kt` and work through each class as it comes up
	* Run server
		* Go to Postman, post against `localhost:8080/encode` and `localhost:8080/decode`
	* Create Objects
		* `cd git/kubernetes-demo/k8s-demo-service/`
		* `kubectl apply -f service.yaml`
		* `kubectl apply -f pod.yaml`
	* Check server
		* `kubectl get pods`
		* `kubectl get svc`
			* Get nodeport
		* Same as above to check end points

* K8s Demo Frontend
	* Web page 
		* Show `App.js`, `Languages.js`, `Base64.js`, and `server.js`
	* Run server
		* Go to Postman, post against `localhost:8080/encode` and `localhost:8080/decode`
	* Create Objects
		* `cd git/kubernetes-demo/k8s-demo-service/`
		* `kubectl apply -f service.yaml`
		* `kubectl apply -f pod.yaml`
	* Check server
		* `kubectl get pods`
		* `kubectl get svc`
			* Get nodeport
		* Same as above to check end points
